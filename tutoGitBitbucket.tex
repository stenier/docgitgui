\documentclass[a4paper,10pt]{article}
\usepackage[utf8]{inputenc}
\usepackage{hyperref}
\usepackage{graphicx,xcolor} 
\usepackage{listings} 
\usepackage[francais]{babel}

\definecolor{editorGray}{rgb}{0.95, 0.95, 0.95}
\definecolor{editorOcher}{rgb}{1, 0.5, 0} % #FF7F00 -> rgb(239, 169, 0)
\definecolor{editorGreen}{rgb}{0, 0.5, 0} % #007C00 -> rgb(0, 124, 0)

\lstdefinelanguage{git}{
  morekeywords={chdir,cd,mv,more,remote,git,clone,commit,push,pull,init,add,mkdir},
  morecomment=[l]{\#},
  morestring=[b]",
}


\lstset{
 % Basic design
      backgroundcolor=\color{editorGray},
      basicstyle=\ttfamily\scriptsize,
      columns=fullflexible, keepspaces,
      frame=l,
	% Line numbers
      numbers=left,
      stepnumber=1,
      firstnumber=1,
      numberfirstline=true,
	% Code design   
      keywordstyle=\color{blue}\bfseries,
      commentstyle=\color{darkgray}\ttfamily,
      ndkeywordstyle=\color{editorGreen}\bfseries,
      stringstyle=\color{editorOcher},
	% Code
      language=git,
      alsodigit={.:;},
      tabsize=2,
      showtabs=false,
      showspaces=false,
      showstringspaces=false,
      breaklines=true,        
      extendedchars=true,
      literate={à}{{\`a}}1 {â}{{\^a}}1 {ã}{{\~a}}1 {é}{{\'e}}1 {è}{{\`e}}1 {ê}{{\^e}}1 {ô}{{\^o}}1
    }

\title{Utilisation de l'outil Git et de la plateforme d'hébergement Bitbucket pour la gestion de projets à l'ESIGELEC}

\author{\href{mailto:s.tenier@groupe-esigelec.fr}{Sylvain Tenier}\\
 Département TIC, ESIGELEC}
 \date{septembre 2016}

 \begin{document}
 \maketitle

 \begin{abstract}
   Ce document a pour but d'initier de futurs ingénieurs à l'utilisation 
   d'outils conventionnellement employés en entreprise dans le cadre de la gestion de projets
   informatique. 
 \end{abstract}

 \tableofcontents

 \part{Mise en place du projet}

 \section{Présentation des outils}\label{sec:presentationBitbucket}

 \emph{Git} est un outil de gestion de code source. Il est couramment associé à une plateforme d'hébergement telle que \emph{bitbucket}, \emph{Github} ou \emph{Gitlab}.

 Git a la particularité d'être \emph{décentralisé}, c'est-à-dire que le projet est présent sur chacune des machines des membres du projet. Ceux-ci peuvent ainsi travailler indépendanmment, sans connexion internet, puis partager le code avec les autres membres de manière régulière. Git assure ainsi la partie communication et publication relative à un projet. A la manière d'un client FTP, il permet de pousser ou de récupérer les fichiers sur un serveur.

 Pour faciliter le partage, une plateforme d'hébergement permet de centraliser le code. Dans le cadre de votre projet à l'Esigelec, vous utiliserez la plateforme située à l'adresse \url{https://bitbucket.org}.

 \section{Création de votre projet sur bitbucket}\label{sec:premier}
 Accédez au site \emph{bitbucket} avec votre navigateur puis : 
 \begin{description}
   \item[Chaque membre du projet] doit créer un compte sur le site en utilisant son adresse email professionnelle (\emph{@groupe-esigelec.fr} dans le cadre de votre scolarité). 
   \item[Un des membres] doit créer un dépôt (``repository'') sur le site. Ce membre devient l'administrateur du dépôt bitbucket. 
 \end{description}
 \begin{figure}[h!] 
   \centering
   \includegraphics[width=0.8\textwidth]{images/bitbucket_vide.PNG} 
   \caption{\label{fig:bitbucketVide} Page d'accueil d'un dépôt bitbucket vide.}
 \end{figure}
 \`A ce point, le dépôt bitbucket est créé mais vide, comme illustré figure \ref{fig:bitbucketVide}. A partir de cette page, l'administrateur doit : 
 \begin{enumerate}
   \item envoyer une invitation via bitbucket aux autres membres du groupe afin de les rattacher au dépôt bitbucket créé. Pour cela, allez dans le menu \emph{settings} accessible par une icône à gauche de votre dépôt bitbucket puis cliquez sur ``gestion des accès'' (ou \emph{Access Management})\par
     \colorbox{pink}{\hbox to \textwidth{Veillez à fournir aux membres concernés les droits d'accès en écriture.\hfill}}
     \item envoyer une invitation à l'enseignant chargé du suivi de votre projet en lui fournissant uniquement les droits d'accès en lecture. 

 \end{enumerate}

 Une fois ces étapes réalisées, vous disposez d'un espace commun pour déposer et partager entre les membres du groupe le code source et les documents relatifs à votre projet. Cet espace est accessible via Internet depuis l'Esigelec et depuis chez vous. Lisez la section suivante pour installer le logiciel permettant cette gestion de projet.

 \section{Création de votre projet sur votre machine}\label{sec:creation}

 Créez un document avec votre logiciel de traitement de textes favori (Word, LibreOffice, \ldots). Pour la suite de ce document, nous considérerons que ce premier document s'intitule \texttt{projet.odt} et que vous l'avez enregistré dans le répertoire \texttt{X:\textbackslash projet}.

 \begin{enumerate}
   \item Lancez un terminal (programme \emph{Invite de commande}, ou recherchez la commande \texttt{cmd}).
  \item Allez dans le répertoire contenant votre projet. Dans notre exemple, vous saisirez les lignes suivantes dans le terminal :
\begin{lstlisting}[language=git]
X:
cd X:\projet
\end{lstlisting}
   \item Transformez votre répertoire de travail en \emph{dépôt} Git en saisissant la commande \lstinline$git init$
   \item Reliez votre dépôt nouvellement créé au site bitbucket en utilisant la commande commençant par \lstinline$git remote add$ \ldots  que vous récupérez en vous connectant à votre projet sur le site bitbucket  (commande encadrée sur la figure \ref{fig:bitbucketVide}). 
   \item Faites un premier envoi de votre document. Dans notre premier exemple, vous devez saisir la série de commandes suivantes :
\begin{lstlisting}
git add projet.odt
git commit -m 'Envoi initial du document de conception'
git push -u origin master
\end{lstlisting}
 \end{enumerate}

 Une fois l'envoi réalisé avec succès, rafraichissez la page du projet sur bitbucket. Votre fichier devrait apparaitre dans la partie \emph{Code Source} du site.
 La suite de ce document décrit le fonctionnement de Git pour l'ensemble des membres du groupe.
 \newpage
 \part{Suivi du projet}

 \section{Clonage du  projet sur une nouvelle machine}

 L'opération de \emph{clonage} est à effectuer une seule fois par machine, dans les cas suivants :
 \begin{itemize}
   \item par les autres membres du groupe sur leur propre machine,
   \item par tout membre du groupe sur une nouvelle machine.
 \end{itemize}

 Cette opération récupère l'intégralité du projet et crée un dépôt local sur votre machine. Pour réaliser le clonage
 \begin{enumerate}
   \item Connectez-vous avec votre propre compte sur le projet
   \item Récupérez l'url de clonage. Celle-ci est de la forme \texttt{https://votrelogin@lurlduprojet.git}
   \item Lancez un terminal (programme \emph{Invite de commande}, ou recherchez la commande \texttt{cmd}).
   \item Nous allons prendre l'exemple du clonage d'un projet à l'intérieur d'un répertoire existant situé à \texttt{X:\textbackslash MOSW}
\begin{lstlisting}
X:
cd X:\MOSW
git clone https:\\votrelogin@lurlduprojet.git
\end{lstlisting}
 \end{enumerate}

Vous devriez récupérer la dernière version de votre projet dans un nouveau répertoire portant le nom de votre projet. Vous allez désormais travailler à l'intérieur de ce répertoire qui fait également office de dépôt Git.

\section{Opérations courantes de suivi de projet}

Une fois le projet cloné sur votre machine, vous aurez 3 opérations à effectuer : envoyer votre travail sur bitbucket, récupérer le travail de vos collègues depuis bitbucket, et gérer les conflits si vous avez travaillé à plusieurs sur le même fichier.

\subsection{Envoi du travail sur bitbucket}\label{subsec:envoi}

Cette opération consiste à identifier les fichiers que vous avez ajouté et modifier, puis à créer un commit et enfin à transférer sur bitbucket. L'exemple suivant considère que vous avez ajouté un fichier \texttt{readme.txt} et modifié le fichier \texttt{conception.odt}. Lancez un terminal (programme \emph{Invite de commande}, ou recherchez la commande \texttt{cmd}).
\begin{lstlisting}
cd <votre répertoire de travail>
git add readme.txt #prise en compte du fichier
git add conception.odt #prise en compte du fichier
git commit -m "modification du fichier de conception et ajout d'un readme" #enregistrement local d'un commit, prêt à être envoyé au serveur
}
git push #envoi au serveur bitbucket
\end{lstlisting}

Si l'opération est un succès, vous pouvez voir sur bitbucket
\begin{itemize}
  \item Les fichiers à jour dans l'onglet \emph{Source}
  \item L'ensemble des modifications causées par le commit dans l'onglet \emph{Commits} 
\end{itemize}

Si l'opération échoue, c'est certainement qu'un autre membre du groupe a effectué une mise-à-jour avant vous. Pour résoudre le problème, vous devez vous-même mettre-à-jour avant d'envoyer. Lisez la section suivante pour voir comment procéder.

\subsection{Récupération des modifications depuis le serveur}

Cette opération consiste à mettre à jour votre répertoire de travail déjà existant (suite à un clonage) sur votre machine depuis le serveur bitbucket. Vous devez réaliser cette opération si un de vos collègues à mis-à-jour le code, ou si vous avez travaillé sur une autre machine. Lancez un terminal (programme \emph{Invite de commande}, ou recherchez la commande \texttt{cmd}).

\begin{lstlisting}
cd <votre répertoire de travail>
git pull
\end{lstlisting}

Si l'opération est un succès, vous pouvez vous-même envoyer vos modifications, puis vous remettre à coder. Sinon, c'est qu'il y a un conflit. Pas de stress, c'est juste que votre groupe travaille fort !

\subsection{Gestion des conflits}

Un conflit peut se produire si 2 membres du groupe ont mis-à-jour le même fichier indépendamment. Le premier à envoyer son code sur bitbucket n'aura pas de problème, mais le 2\up{ème} devra résoudre le conflit. Excellente nouvelle, Git vient de vous éviter d'écraser le travail d'un autre membre du groupe !

Pour résoudre le conflit, c'est très simple.
\begin{enumerate}
  \item Ouvrez l'outil \emph{Meld} installé sur les machines de l'école. Si vous êtes sur votre machine, téléchargez et installez-le.
  \item Supprimez les lignes en conflit
  \item Envoyez le fichier fusionné sur bitbucket dans un nouveau commit (voir section \ref{subsec:envoi})
\end{enumerate}

\section{Conclusion}

Git et bitbucket sont des outils précieux pour votre travail. La maitrise d'un outil de gestion des versions est de plus en plus demandée en entreprise, utilisez ces outils sérieusement et pensez à le mentionner sur votre CV !

Bon codage.

 \end{document}
